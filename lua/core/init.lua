require("core.lsp")
require("core.autocmd")
require("core.mappings")
require("core.options")
